﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BankQueue.ViewModels;
namespace BankQueue.Controllers
{
    public class QueueController : Controller
    {
        // GET: Queue
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult Calculate(int number)
        {
            //initial customer sample data
            List<CustomerViewModel> customer = new List<CustomerViewModel>(InitialCustomer());

            // initial queue by input value 
            List<QueueViewModels> queue = new List<QueueViewModels>(InitialQueue(number));


            // initial process type 
            List<TypeViewModels> type = new List<TypeViewModels>(InitialType());

            foreach (var cus in customer)
            {
                //queue.IndexOf(

                // 讓手續ID 找到 對應的 手續名稱
                var typeR = type.SingleOrDefault(a => a.Id == cus.Type);
                // 找到目前等待時間最少的窗口
                var min = queue.Aggregate((i1, i2) => i1.Time < i2.Time ? i1 : i2);


                // add 辦手續的客戶到 list 
                min.Customer.Add(new CustomerViewModel
                {
                    Name = cus.Name,
                    TypeName = typeR.Name,
                    Type = cus.Type
                });
                // update total time
                min.Time += typeR.Time;

            }


            return Json(queue, JsonRequestBehavior.AllowGet);
        }

        private List<TypeViewModels> InitialType()
        {
            List<TypeViewModels> list = new List<TypeViewModels> {
                new TypeViewModels()
                {
                    Id = 1,
                    Name = "存款",
                    Time = 3
                },
                 new TypeViewModels()
                 {
                     Id = 2,
                     Name = "提款",
                     Time = 5
                 },
                  new TypeViewModels()
                  {
                      Id = 3,
                      Name = "開戶",
                      Time = 10
                  },
            };

            return list;
        }

        private List<CustomerViewModel> InitialCustomer()
        {

            List<CustomerViewModel> queue = new List<CustomerViewModel>
            {
                    new CustomerViewModel()
                    {
                        Type = 2,
                        Name = "炳中"
                    },
                    new CustomerViewModel
                    {
                        Type = 1,
                        Name = "張妥"
                    },
                    new CustomerViewModel
                    {
                        Type = 1,
                        Name = "富強"
                    },
                    new CustomerViewModel
                    {
                        Type = 2,
                        Name = "永光"
                    },
                    new CustomerViewModel
                    {
                        Type = 3,
                        Name = "醒民"
                    },
                    new CustomerViewModel
                    {
                        Type = 1,
                        Name = "和昌"
                    },
                    new CustomerViewModel
                    {
                        Type = 2,
                        Name = "博玄"
                    },
                    new CustomerViewModel
                    {
                        Type = 2,
                        Name = "雯霞"
                    },
                    new CustomerViewModel
                    {
                        Type = 3,
                        Name = "嘉伶"
                    },
                    new CustomerViewModel
                    {
                        Type = 1,
                        Name = "文慶"
                    }
            };
            return queue;
        }

        private List<QueueViewModels> InitialQueue(int queueAmount)
        {
            List<QueueViewModels> list = new List<QueueViewModels>();
            for (int index = 0; index < queueAmount; index++)
            {
                list.Add(new QueueViewModels
                {
                    QueueId = index + 1,
                    Time = 0,
                    Customer = new List<CustomerViewModel>()
                });
            }

            return list;
        }
    }
}