﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankQueue.ViewModels
{
    public class CustomerViewModel
    {
        public int Type { get; internal set; }
        public string Name { get; internal set; }
        public string TypeName { get; internal set; }
    }
}