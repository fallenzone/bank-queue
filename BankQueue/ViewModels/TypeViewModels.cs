﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankQueue.ViewModels
{
    public class TypeViewModels
    {
        public int Id { set; get; }
        public string Name{set;get;}
        public int Time { get; internal set; }
    }
}