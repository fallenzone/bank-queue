﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BankQueue.ViewModels
{
    public class QueueViewModels
    {
        public List<CustomerViewModel> Customer { get; set; }
        public int QueueId { get; set; }
        public int Time { get; set; }
    }
}